nightwatch_config = {
 "src_folders" : ["nw/tests/"],
 "selenium" : {
 "start_process" : false,
 "host" : "hub.browserstack.com",
 "port" : 80
 },
 "common_capabilities": {
  "browserstack.user": process.env.BROWSERSTACK_USER,
  "browserstack.key": process.env.BROWSERSTACK_KEY,
 },

"test_settings": {
 "default": {},
 "chrome": {
 "desiredCapabilities": {
 "browser": "chrome"
 }
 },
 "firefox": {
 "desiredCapabilities": {
 "browser": "firefox"
 }
 },
 "ie": {
 "desiredCapabilities": {
 "browser": "internet explorer"
 }
 }
 }
}

// Code to support common capabilites
for(var i in nightwatch_config.test_settings){
 var config = nightwatch_config.test_settings[i];
 config['selenium_host'] = nightwatch_config.selenium.host;
 config['selenium_port'] = nightwatch_config.selenium.port;
 config['desiredCapabilities'] = config['desiredCapabilities'] || {};
 for(var j in nightwatch_config.common_capabilities){
 config['desiredCapabilities'][j] = config['desiredCapabilities'][j] || nightwatch_config.common_capabilities[j];
 }
}

module.exports = nightwatch_config;