import React from 'react'
import {Button} from 'react-bootstrap'
import './Todo.css'


function Todo(props) {
  return (
    <li key={props.id} className="todoItem">
      {props.text} 

      <Button className="btn-danger btn-sm todoDelete" onClick={ () => props.onDeleteItem(props.id) }>supprimer</Button>
    </li>
  )
}

export default Todo;