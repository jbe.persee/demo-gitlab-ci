import React, {Component} from 'react'

import {Button } from 'react-bootstrap'

class TodoForm extends Component{

  submitForm(e){
    e.preventDefault()
    let todo = this.refs.todoText.value
    this.refs.todoText.value = '';
    this.props.onSubmitForm(todo)
  }

  render(){
    return (
      <form onSubmit={(e) => this.submitForm(e) } type="submit">
        <h2>Ajouter une todo</h2>
        <input type="text" placeholder="A faire" ref="todoText" required />
        <Button type="submit" className="btn-primary btn-sm">Créer</Button>
      </form>
    )
  }
}

export default TodoForm;